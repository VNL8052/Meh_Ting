#include<bits/stdc++.h>
using namespace std;
 
class MyBigNumber
{
	public:
	string sum(string stn1, string stn2)
	{
	    // Before proceeding further, make sure length of str2 is larger.
	    if (stn1.length() > stn2.length())
	        swap(stn1, stn2);
	 
	    // Take an empty string for storing result
	    string str = "";
	 
	    // Calculate length of both string
	    int n1 = stn1.length(), n2 = stn2.length();
	 
	    // Reverse both of strings
	    reverse(stn1.begin(), stn1.end());
	    reverse(stn2.begin(), stn2.end());
	 
	    int carry = 0;
	    for (int i=0; i<n1; i++)
	    {
	        // Do school mathematics, compute sum of current digits and carry
	        int sum = ((stn1[i]-'0')+(stn2[i]-'0')+carry);
	        str.push_back(sum%10 + '0');
	 
	        // Calculate carry for next step
	        carry = sum/10;
	    }
	 
	    // Add remaining digits of larger number
	    for (int i=n1; i<n2; i++)
	    {
	        int sum = ((stn2[i]-'0')+carry);
	        str.push_back(sum%10 + '0');
	        carry = sum/10;
	    }
	 
	    // Add remaining carry
	    if (carry)
	        str.push_back(carry+'0');
	 
	    // reverse resultant string
	    reverse(str.begin(), str.end());
	 
	    return str;
	}
};

 
// Driver code
int main()
{
	MyBigNumber s1;
    string str1 = "1234";
    string str2 = "897";
    printf("%s", s1.sum(str1, str2).c_str());
    //ans = 2131
    return 0;
}
