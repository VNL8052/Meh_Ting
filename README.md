CÁCH CHẠY FILE MyBigNumber:
- Step 1: mở file lên.
- Step 2: Kéo xuống line 55-56 và chỉnh value của 2 giá trị str1/str2 thành con số mong muốn.
- Step 3: Chạy trình biên dịch(ở đây mình dùng VS Code), ở màn hình output sẽ là đáp án sau khi cộng 2 số.

-Đáp án hiện tại ở file .java khi cộng 1 và 2^64 = 18,446,744,073,709,551,617.

-Đáp án hiện tại ở file .cpp khi cộng 1234 và 897 = 2131.
