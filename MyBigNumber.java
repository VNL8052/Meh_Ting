import java.util.*;

class MyBigNumber
{
    static String sum(String stn1, String stn2)
    {
        if (stn1.length() > stn2.length()){
            String t = stn1;
            stn1 = stn2;
            stn2 = t;
        }
        String str = "";
    
        // Calculate length of both String
        int n1 = stn1.length(), n2 = stn2.length();
    
        // Reverse both of Strings
        stn1=new StringBuilder(stn1).reverse().toString();
        stn2=new StringBuilder(stn2).reverse().toString();
    
        int carry = 0;
        for (int i = 0; i < n1; i++)
        {
            // Do school mathematics, compute sum of
            // current digits and carry
            int sum = ((int)(stn1.charAt(i) - '0') +
                        (int)(stn2.charAt(i) - '0') + carry);
            str += (char)(sum % 10 + '0');
    
            // Calculate carry for next step
            carry = sum / 10;
        }
    
        // Add remaining digits of larger number
        for (int i = n1; i < n2; i++)
        {
            int sum = ((int)(stn2.charAt(i) - '0') + carry);
            str += (char)(sum % 10 + '0');
            carry = sum / 10;
        }
    
        // Add remaining carry
        if (carry > 0)
            str += (char)(carry + '0');
    
        // reverse resultant String
        str = new StringBuilder(str).reverse().toString();
    
        return str;
    }
}